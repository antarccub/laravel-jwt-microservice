<?php

namespace Tests\Feature;

use Lcobucci\JWT\Signer\Rsa\Sha256;
use Lcobucci\JWT\Token;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TokenVerifyTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testJWTwithCorrectPKey(){

        $token = $this->getJWT();

        $this->assertTrue($token->verify(new Sha256(), $this->getPublicKey()));

    }

    public function testJWTwithIncorrectPKey(){

        $token = $this->getJWT();

        $this->assertNotTrue($token->verify(new Sha256(), $this->getFalsePublicKey()));

    }
}
