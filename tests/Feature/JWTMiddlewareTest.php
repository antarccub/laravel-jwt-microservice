<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class JWTMiddlewareTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testcheck_with_correct_token()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$this->getJWT(),
        ])->json('GET', '/');

        $response
            ->assertStatus(200);
    }
}
