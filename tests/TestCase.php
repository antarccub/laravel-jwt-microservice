<?php

namespace Tests;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Rsa\Sha256;

use phpseclib\Crypt\RSA;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    private $RSA_Keys;

    private $JWT_Token;

    public function __construct(string $name = null, array $data = [], string $dataName = '')
    {
        $this->issueTestJWT();
        parent::__construct($name, $data, $dataName);
    }

    public function getJWT(){

        return $this->JWT_Token;
    }

    protected function issueTestJWT($expirationDateTime = null, $subject = 1, $scopes = [], $issuer = null, $client = 1){
        $builder = (new Builder())
            ->setAudience($client)
            ->setId(str_random(), true)
            ->setIssuedAt(time())
            ->setNotBefore(time())
            ->setExpiration($expirationDateTime ?: Carbon::now()->addDay()->getTimestamp())
            ->setSubject($subject)
            ->set('scopes', $scopes)
            ->set('email', "test@gitlab.com")
            ->set('role', "TESTER")
            ->set('iss', $issuer ?: 'https://localhost');
        // sign and return the token

        $this->JWT_Token =  $builder->sign(new Sha256(), $this->getPrivateKey())->getToken();
    }

    public function getPublicKey(){

        if(!$this->RSA_Keys){
            $this->bootstrapRSAKeys();
        }

        return new Key($this->RSA_Keys['publickey']);

    }

    public function getFalsePublicKey(){


        $rsa = new RSA();
        $falseKeys = $rsa->createKey();

        return new Key($falseKeys['publickey']);

    }

    public function getPrivateKey(){

        if(!$this->RSA_Keys){
            $this->bootstrapRSAKeys();
        }
        return new Key($this->RSA_Keys['privatekey']);
    }

    private function bootstrapRSAKeys(){

        $rsa = new RSA();
        $this->RSA_Keys = $rsa->createKey();

    }

}
